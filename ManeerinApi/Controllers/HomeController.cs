﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ManeerinApi.Controllers
{
    [Route("api/HomeController")]
    public class HomeController : ControllerBase
    {
        [HttpPost("v1/link-richmenu")]
        public IActionResult LinkRichMenu(string userId)
        {
            try
            {
                string channelAccessToken = "093vQgLGPejtDK4PUrn8w7DMJqL6JKNQgcA7JmRmPnRRbnsb9nTCPYeJbbZXnsHIAdIlFWVwzXTjAqi9nO/A24u5GyrCQhbAqlQIZcE9mGT27srDFtGFmStZTzGF9/+1BXN0r4aNqMTL8lA78yJ+UAdB04t89/1O/w1cDnyilFU=";

                isRock.LineBot.Utility.LinkRichMenuToUser("richmenu-c0f251850a9fe0d1f101cc1af0d62751", userId, channelAccessToken);

                return Ok(new { StatusCode = (int)HttpStatusCode.OK, Message = "เปลี่ยนเมนูสำเร็จ !" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { IsSuccess = false, ErrorMessage = ex.Message, ErrorCode = (int)HttpStatusCode.InternalServerError });
            }
        }
    }
}
